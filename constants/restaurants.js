export const PIVNICE_U_CAPA = {
    name: 'Pivnice u čápa',
    url: 'https://www.menicka.cz/2700-pivnice-u-capa.html'
}
export const SUZIES = {
    name: 'Suzies steak pub',
    url: 'https://www.menicka.cz/3830-suzies-steak-pub.html'
}
export const VERONI = {
    name: 'VERONI coffee & chocolate',
    url: 'https://www.menicka.cz/4921-veroni-coffee--chocolate.html'
}

export const RESTAURANTS = [PIVNICE_U_CAPA, SUZIES, VERONI]

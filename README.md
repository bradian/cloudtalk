# Dnešné menu

Zadanie na interview pre CloudTalk

## Inštalácia

```
 npm i
```

## Spustenie

```
 npm start
```

## Ako používať?

*localhost:3000* pre získanie dnešných jedálnych lístkov z reštaurácii:
- Pivnice U Čápa
- Suzies steak pub
- VERONI coffee & chocolate

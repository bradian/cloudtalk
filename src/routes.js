import express from 'express'
import {todayMenus} from './controllers/restaurants.js'
import {catchExceptions} from './utils.js'


const router = express.Router()

router.get('/', catchExceptions(todayMenus))

export default router

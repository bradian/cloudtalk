import fetch from 'node-fetch'
import iconv from 'iconv-lite'
import mapValues from 'lodash/mapValues.js'
import map from 'lodash/map.js'

export const catchExceptions = (callback) => async (req, res, next) => {
    try {
        await callback(req, res)
    } catch (e) {
        next(e)
    }
}

export const decodedPage = async (url) =>  {
    const page = await fetch(url)
    return iconv.decode(Buffer.from(await page.arrayBuffer()), 'win1250');
}

export const processMeals = async (loadedMeals) => (
    mapValues(loadedMeals, (meals) => map(meals, (meal) => (
        {
            description: meal.querySelector('.polozka').textContent,
            price: meal.querySelector('.cena')?.textContent || '0 Kč',
        }
    )))
)

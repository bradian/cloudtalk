import {JSDOM} from 'jsdom'
import map from 'lodash/map.js'
import {RESTAURANTS} from '../../constants/restaurants.js'
import {decodedPage, processMeals} from '../utils.js'

export const todayMenus = async (req, res) => {
    const restaurants = await Promise.all(map(RESTAURANTS, async (restaurant) => {
        const page = await decodedPage(restaurant.url)
        const {window: {document}} = new JSDOM(page)

        const loadedMenu = document.querySelector('.menicka')
        const loadedMeals = {
            soups: loadedMenu.getElementsByClassName('polevka'),
            mains: loadedMenu.getElementsByClassName('jidlo'),
        }

        return {
            name: restaurant.name,
            menu: (loadedMeals.mains.length > 0) ? await processMeals(loadedMeals) : 'Dnes neponúkame denné menu.'
        }
    }))

    res.json({
        date: new Date().toDateString(),
        restaurants
    })
}
